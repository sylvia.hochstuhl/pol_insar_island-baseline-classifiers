import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os
import glob
from sklearn.model_selection import cross_validate, GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report, ConfusionMatrixDisplay
import sys
import pickle as pkl
from feature_extraction import feature_extraction_pipeline


class PercentileScaler():
    """Normalize along the feature axis to be in range [0, 1]"""

    def __init__(self, quantile_range=(3, 97)):
        self.quantile_range = quantile_range
        self.p_low = []
        self.p_high = []

    def fit(self, X):
        self.p_low = [np.percentile(X[:, idx][~np.isnan(X[:, idx])], self.quantile_range[0]) for idx in
                      range(X.shape[1])]
        self.p_high = [np.percentile(X[:, idx][~np.isnan(X[:, idx])], self.quantile_range[1]) for idx in
                       range(X.shape[1])]
        return self

    def transform(self, X):
        for idx in range(X.shape[1]):
            X[:, idx] = np.clip(((X[:, idx] - self.p_low[idx]) / (self.p_high[idx] - self.p_low[idx])), 0, 1)
        return X

    def fit_transform(self, X):
        self.p_low = [np.percentile(X[:, idx][~np.isnan(X[:, idx])], self.quantile_range[0]) for idx in
                      range(X.shape[1])]
        self.p_high = [np.percentile(X[:, idx][~np.isnan(X[:, idx])], self.quantile_range[1]) for idx in
                       range(X.shape[1])]
        for idx in range(X.shape[1]):
            X[:, idx] = np.clip(((X[:, idx] - self.p_low[idx]) / (self.p_high[idx] - self.p_low[idx])), 0, 1)
        return X


def point_selection(df, method='random', label='label', sample_size=10000):
    if method == 'random':
        print('Random Selection of training subset')
        return df.sample(n=sample_size, random_state=3).index.values

    elif method == 'label-stratified':
        if label not in df.columns:
            raise ValueError('Label must be given for selection method: label-stratified')
        print('Select points based on label (stratified sampling)')
        return df.groupby(label, group_keys=False).apply(lambda x: x.sample(frac=sample_size / len(df))).index.values

    elif method == 'label-balanced':
        if label not in df.columns:
            raise ValueError('Label must be given for selection method: label-balanced')
        print('Select points based on label (balanced sampling)')
        # Select same number of points from every class
        n_classes = df[label].max()
        n_samples = min([sample_size // n_classes] + df[label].value_counts().to_list())
        return df.groupby(label, group_keys=False).apply(lambda x: x.sample(n_samples)).index.values


# Label names


labels = ['Tidal flat', 'Water', 'Coastal Shrub', 'Dense, high vegetation', 'White dunes beach grass', 'Peat bog',
          'Grey dunes grassland', 'Couch grass', 'Upper salt marsh', 'Lower salt marsh', 'Sand', 'Settlement']

fp01_shape = (3616, 2502)
fp02_shape = (3616, 2540)

feature_set = {'Pol-L': ['alpha_L', 'anisotropy_L',
                         'conformity_L', 'degree_purity_L', 'depolarisation_index_L',
                         'entropy_L', 'lambda_db_L', 'p1_L', 'p2_L', 'p3_L',
                         'scatt_diversity_L', 'scatt_predominance_L', 'T11_db_L',
                         'T12_db_L', 'T12_pha_L', 'T13_db_L', 'T13_pha_L', 'T22_db_L',
                         'T23_db_L', 'T23_pha_L', 'T33_db_L',
                         'Yamaguchi4_Vol_rel_L', 'Yamaguchi4_Dbl_rel_L', 'Yamaguchi4_Odd_rel_L'],
               'Pol-S': ['alpha_S', 'anisotropy_S',
                         'conformity_S', 'degree_purity_S', 'depolarisation_index_S', 'entropy_S',
                         'lambda_db_S', 'p1_S', 'p2_S', 'p3_S', 'scatt_diversity_S',
                         'scatt_predominance_S', 'T11_db_S', 'T12_db_S', 'T12_pha_S', 'T13_db_S',
                         'T13_pha_S', 'T22_db_S', 'T23_db_S', 'T23_pha_S', 'T33_db_S',
                         'Yamaguchi4_Vol_rel_S', 'Yamaguchi4_Dbl_rel_S', 'Yamaguchi4_Odd_rel_S'],
               'Pol-SL': ['alpha_L', 'anisotropy_L',
                          'conformity_L', 'degree_purity_L', 'depolarisation_index_L',
                          'entropy_L', 'lambda_db_L', 'p1_L', 'p2_L', 'p3_L',
                          'scatt_diversity_L', 'scatt_predominance_L', 'T11_db_L',
                          'T12_db_L', 'T12_pha_L', 'T13_db_L', 'T13_pha_L', 'T22_db_L',
                          'T23_db_L', 'T23_pha_L', 'T33_db_L',
                          'Yamaguchi4_Vol_rel_L', 'Yamaguchi4_Dbl_rel_L', 'Yamaguchi4_Odd_rel_L',
                          'alpha_S', 'anisotropy_S',
                          'conformity_S', 'degree_purity_S', 'depolarisation_index_S', 'entropy_S',
                          'lambda_db_S', 'p1_S', 'p2_S', 'p3_S', 'scatt_diversity_S',
                          'scatt_predominance_S', 'T11_db_S', 'T12_db_S', 'T12_pha_S', 'T13_db_S',
                          'T13_pha_S', 'T22_db_S', 'T23_db_S', 'T23_pha_S', 'T33_db_S',
                          'Yamaguchi4_Vol_rel_S', 'Yamaguchi4_Dbl_rel_S', 'Yamaguchi4_Odd_rel_S'],
               'PolIn-S': ['alpha_S', 'anisotropy_S', 'coh_hh_S', 'coh_hv_S', 'coh_vv_S',
                           'conformity_S', 'degree_purity_S', 'depolarisation_index_S', 'entropy_S',
                           'lambda_db_S', 'p1_S', 'p2_S', 'p3_S', 'scatt_diversity_S',
                           'scatt_predominance_S', 'T11_db_S', 'T12_db_S', 'T12_pha_S', 'T13_db_S',
                           'T13_pha_S', 'T22_db_S', 'T23_db_S', 'T23_pha_S', 'T33_db_S',
                           'Yamaguchi4_Vol_rel_S', 'Yamaguchi4_Dbl_rel_S', 'Yamaguchi4_Odd_rel_S'],
               'PolIn-L': ['alpha_L', 'anisotropy_L', 'coh_hh_L', 'coh_hv_L', 'coh_vv_L',
                           'conformity_L', 'degree_purity_L', 'depolarisation_index_L',
                           'entropy_L', 'lambda_db_L', 'p1_L', 'p2_L', 'p3_L',
                           'scatt_diversity_L', 'scatt_predominance_L', 'T11_db_L',
                           'T12_db_L', 'T12_pha_L', 'T13_db_L', 'T13_pha_L', 'T22_db_L',
                           'T23_db_L', 'T23_pha_L', 'T33_db_L',
                           'Yamaguchi4_Vol_rel_L', 'Yamaguchi4_Dbl_rel_L', 'Yamaguchi4_Odd_rel_L'],
               'PolIn-SL': ['alpha_L', 'anisotropy_L', 'coh_hh_L', 'coh_hv_L', 'coh_vv_L',
                            'conformity_L', 'degree_purity_L', 'depolarisation_index_L',
                            'entropy_L', 'lambda_db_L', 'p1_L', 'p2_L', 'p3_L',
                            'scatt_diversity_L', 'scatt_predominance_L', 'T11_db_L',
                            'T12_db_L', 'T12_pha_L', 'T13_db_L', 'T13_pha_L', 'T22_db_L',
                            'T23_db_L', 'T23_pha_L', 'T33_db_L',
                            'Yamaguchi4_Vol_rel_L', 'Yamaguchi4_Dbl_rel_L', 'Yamaguchi4_Odd_rel_L',
                            'alpha_S', 'anisotropy_S', 'coh_hh_S', 'coh_hv_S', 'coh_vv_S',
                            'conformity_S', 'degree_purity_S', 'depolarisation_index_S', 'entropy_S',
                            'lambda_db_S', 'p1_S', 'p2_S', 'p3_S', 'scatt_diversity_S',
                            'scatt_predominance_S', 'T11_db_S', 'T12_db_S', 'T12_pha_S', 'T13_db_S',
                            'T13_pha_S', 'T22_db_S', 'T23_db_S', 'T23_pha_S', 'T33_db_S',
                            'Yamaguchi4_Vol_rel_S', 'Yamaguchi4_Dbl_rel_S', 'Yamaguchi4_Odd_rel_S']}


def rf_train_cv(df, feature_set_name, out_dir):
    feature_list = feature_set[feature_set_name]

    # Prepare train data
    shuffled = df.sample(frac=1)
    X_train = shuffled.loc[shuffled['label_train'] != 0, feature_list]
    y_train = shuffled.loc[shuffled['label_train'] != 0, 'label_train']

    scaler = PercentileScaler(quantile_range=(1, 99))
    X_train_scaled = scaler.fit_transform(X_train.to_numpy())

    # Save scaler
    with open(os.path.join(out_dir, 'scaler.pkl'), 'wb') as f:
        pkl.dump(scaler, f)

    # Random Forest classification including 5-fold-cv for hyperparameter-tuning
    clf5 = GridSearchCV(estimator=RandomForestClassifier(random_state=42, n_jobs=48),
                       param_grid={'n_estimators': [100, 300],
                                   'class_weight': ['balanced', 'balanced_subsample', None]},
                       scoring='balanced_accuracy', verbose=True, cv=5)
    clf10 = GridSearchCV(estimator=RandomForestClassifier(random_state=42, n_jobs=48),
                        param_grid={'n_estimators': [100, 300],
                                    'class_weight': ['balanced', 'balanced_subsample', None]},
                        scoring='balanced_accuracy', verbose=True, cv=10)
    clf5.fit(X_train_scaled, y_train)
    clf10.fit(X_train_scaled, y_train)
    # Print CV results
    print('Best parameters:', clf5.best_params_)
    print('Best parameters:', clf10.best_params_)

    print('Results:', clf5.cv_results_)
    print('Results:', clf10.cv_results_)
    # Save results
    with open(os.path.join(out_dir, feature_set_name + 'best_clf.pkl'), 'wb') as f:
        pkl.dump(clf5.best_estimator_, f)
    with open(os.path.join(out_dir, feature_set_name + '_clf_cv_results.pkl'), 'wb') as f:
        pkl.dump(clf5.cv_results_, f)
    return scaler, clf5.best_estimator_


def rf_train(df, feature_set_name, out_dir, params, save=True):
    # Read Input data
    feature_list = feature_set[feature_set_name]

    # Prepare train and test data (using all available labels)
    X_train = df.loc[df['label_train'] != 0, feature_list]
    y_train = df.loc[df['label_train'] != 0, 'label_train']

    scaler = PercentileScaler(quantile_range=(1, 99))
    X_train_scaled = scaler.fit_transform(X_train.to_numpy())

    # Save scaler
    if save:
        with open(os.path.join(out_dir, 'scaler.pkl'), 'wb') as f:
            pkl.dump(scaler, f)

    # Random Forest classification
    clf = RandomForestClassifier(random_state=42, n_jobs=48, **params)
    clf.fit(X_train_scaled, y_train)
    return scaler, clf


def rf_test(df, feature_set_name, clf, scaler, out_dir):
    if isinstance(df, str):
        df = pd.read_feather(df)
    feature_list = feature_set[feature_set_name]
    X_test = df.loc[df['label_test'] != 0, feature_list]
    y_test = df.loc[df['label_test'] != 0, 'label_test']
    X_test_scaled = scaler.transform(X_test.to_numpy())
    # Plot Confusion matrix
    fig, ax = plt.subplots(figsize=(16, 16))
    disp = ConfusionMatrixDisplay.from_estimator(clf, X_test_scaled, y_test, display_labels=labels, ax=ax,
                                                 cmap=plt.cm.Blues, normalize=None)
    disp.ax_.set_title('Random Forest classification')
    with open(os.path.join(out_dir, feature_set_name + '_confusion_matrix.pkl'), 'wb') as f:
        pkl.dump(disp, f)
    plt.savefig(os.path.join(out_dir, feature_set_name + '_confusion_matrix.svg'))

    val_score = clf.score(X_test_scaled, y_test)

    # classify all image pixels
    X = scaler.transform(df.loc[:, feature_list].to_numpy())
    pr = clf.predict(X)

    # Plot classified images
    fp01 = np.reshape(pr[:fp01_shape[0] * fp01_shape[1]], fp01_shape)
    fp02 = np.reshape(pr[fp01_shape[0] * fp01_shape[1]:], fp02_shape)

    fig, ax = plt.subplots(figsize=(16, 16))
    im = ax.imshow(fp01)
    plt.show()

    fig, ax = plt.subplots(figsize=(16, 16))
    im = ax.imshow(fp02)
    plt.show()

    np.save(os.path.join(out_dir, feature_set_name + '_fp01'), fp01)
    np.save(os.path.join(out_dir, feature_set_name + '_fp02'), fp02)


if __name__ == '__main__':
    input_path = sys.argv[2]        # path to the dataset directory
    output_dir = sys.argv[3]        # Path to store results
    feature_name = sys.argv[4]      # Selected feature set defined above (feature_set)

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    if os.path.isfile(os.path.join(input_path, 'data', 'df.ftr')):
        df = pd.read_feather(os.path.join(input_path, 'data', 'df.ftr'))
        df = df.set_index('index')
        df.interpolate(inplace=True) # interpolate nan-Values

    else:
        df = feature_extraction_pipeline(os.path.join(input_path, 'data'), os.path.join(input_path, 'label'))
        df.interpolate(inplace=True)  # interpolate nan-Values

    if os.path.isfile(os.path.join(output_dir, feature_name + '_clf_cv_results.pkl')):
        with open(os.path.join(output_dir, feature_name + '_clf_cv_results.pkl'), 'rb') as f:
            cv = pkl.load(f)
        rf_params = cv['params'][cv['rank_test_score'][0]]
        scaler, clf = rf_train(df, feature_name, output_dir, rf_params)

    else:
        scaler, clf = rf_train_cv(df, feature_name, output_dir)

    rf_test(df, feature_name, clf, scaler, output_dir)

# Baseline_classifiers



## Description

This project contains python scripts to train and test a Random Forest and a supervised Wishart classifier [[1]](#1) on the Pol-InSAR-Island dataset provided on [KITOpenData](https://dx.doi.org/10.35097/1700) [[2]](#2) and described in "Pol-InSAR-Island - A benchmark dataset for multi-frequency Pol-InSAR data land cover classification" (reference and link will follow after publication).

## Requirements
Use of the provided scripts requires installation of the open-source ESA PolSARpro toolbox, which is used for speckle filtering and polarimetric feature extraction. The software can be downloaded here: [PolSARPro-Toolbox](https://ietr-lab.univ-rennes1.fr/polsarpro-bio/)

Additionally, the following Python packages are required:<br>
numpy<br>
pandas<br>
sklearn<br>
matplotlib<br>
seaborn<br>

## Usage

For training und testing the **Random Forest classifier** use the command:<br>
`python random_forest_classification.py <polsarpro_dir> <PolInSAR-Island-dataset_dir> <output_dir> <featureset>`

polsarpro_dir: Path to PolSARPro's files (e.g.: "C:\\Program Files (x86)\\PolSARpro_v6.0_Biomass_Edition\\Soft/bin")<br>
PolInSAR-Island-dataset_dir: Path to the directory containing the PolInSAR-Island dataset<br>
output_dir: Directory, that will contain the classification output<br>
featureset: Specify which kind of features should be used for classification. The options are: 'Pol-L' (polarimetric features of L-band data), 'Pol-S' (polarimetric features of S-band data), 'Pol-SL' (polarimetric features of S- and L-band data), 'PolIn-L' (polarimetric and interferometric features of L-band data), 'PolIn-S' (polarimetric and interferometric features of S-band data), 'PolIn-SL' (polarimetric and interferometric features of S- and L-band data)

The script will extract polarimetric features (if not already available) and safe it as pandas dataframe (df.ftr) in the data directory. Subsequently a Random Forest classifier is trained (including hyperparamter-tuning) and validated on held-out test data.
The best hyperparameter set and the resulting best performing classifier is stored in the output directory. Additionally the confusion matrix based on classification of the test data is stored in the output directory.Random

For training and testing the **Wishart- classifier** use the command:<br>
`python wishart.py <polsarpro_dir> <PolInSAR-Island-dataset_dir> <output_dir>`

The script will train a supervised Wishart classifier based on all training samples and test it on held-out test data. The results are stored in the specified output directory.

It is also possible to perform training on a class-balanced subset of the training samples. To choose this option, the flag -m must be set:<br>
`python wishart.py <polsarpro_dir> <PolInSAR-Island-dataset_dir> <output_dir>`

## References

<a id="1">[1]</a> J. S. Lee, M. R. Grunes, R. Kwok (1994): Classification of multi-look polarimetric SAR imagery based on complex Wishart distribution. International Journal of Remote Sensing, Vol. 15, No 11, pages 2299–2311.<br>
<a id="2">[2]</a> S. Hochstuhl, N. Pfeffer, A. Thiele, J. Amao-Oliva, R. Scheiber, A. Reigber, H. Dirks (2023): Pol-InSAR-Island - A Benchmark Dataset for Multi-frequency Pol-InSAR Data Land Cover Classification (Version 2). Karlsruhe Institute of Technology. DOI: 10.35097/1700


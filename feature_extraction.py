import os
import subprocess
import numpy as np
import io
import glob
import pandas as pd
import shutil
import sys
import re

polsarpro = sys.argv[1]

rx_dict = {
    'ncol': re.compile(r'samples += (?P<ncol>\d+)'),
    'nrow': re.compile(r'lines += (?P<nrow>\d+)'),
    'ndim': re.compile(r'bands += (?P<ndim>\d)'),
    'datatype': re.compile(r'data type += (?P<datatype>\d+)')
}

datatype_dict = {
    1: np.dtype('uint8'),
    2: np.int,
    3: np.long,
    4: np.float32,
    5: np.float64,
    6: np.complex,
    8: np.complex64
}


def _parse_line(line):
    """
    Do a regex search against all defined regexes and
    return the key and match result of the first matching regex

    """

    for key, rx in rx_dict.items():
        match = rx.search(line)
        if match:
            return key, match
    # if there are no matches
    return None, None


def parse_hdr_header(filename):
    """ Parsing image dimension and datatype from hdr-file

    Parameters
    ----------
    filename: str
        Path to hdr-File

    Returns
    -------
    ncol: int
        number of cols
    nrow: int
        number of rows
    ndim: int
        number of channels
    datatype: np.dtype
        datatype
    """
    with io.open(filename, 'rt', newline='\n') as hdr:
        lines = hdr.readlines()
        for line in lines:
            key, match = _parse_line(line)
            if key == 'ncol':
                ncol = int((match.group('ncol')))
            if key == 'nrow':
                nrow = int(match.group('nrow'))
            if key =='datatype':
                datatype = datatype_dict[int(match.group('datatype'))]
            if key == 'ndim':
                ndim = int(match.group('ndim'))
    return ncol, nrow, ndim, datatype


def read_bin(input_file):
    """ Read binary files (.bin)

    Parameters
    ----------
    input_file: str
        Path to .bin-file

    Return
    ------
    arr: np.array
        numpy array with dimension and type specified in corresponding hdr-file.
    """

    if os.path.splitext(input_file)[-1] != '.bin':
        raise FileNotFoundError
    hdr_file = input_file + '.hdr'
    ncol, nrow, ndim, datatype = parse_hdr_header(hdr_file)
    with open(input_file, 'rb') as f:
        b = f.read()
    return np.frombuffer(b, dtype=datatype).reshape((nrow, ncol))


def write_config(dir, nlig, ncol, polarcase='monostatic', polartype='full'):
    """ Write PolSARPro config file and save in given directory

    Parameters
    ----------
    dir: str
        Path to directory to save config File

    nlig: int
        number of rows

    ncol: int
        number of cols

    polarcase: str (optional, default 'monostatic')
        'bistatic' or 'monostatic'

    polartype: str (optional, default 'full')
        Specify polartype. 'full' or 'dual'
    """

    with open(os.path.join(dir, 'config.txt'), 'w') as f:
        f.writelines(["Nrow\n", str(nlig) + "\n", "---------\n", "Ncol\n", str(ncol)+"\n",
                      "---------\n", "PolarCase\n", polarcase + "\n", "---------\n",
                      "PolarType\n", polartype + "\n"])


def read_config(config_file):
    """Read number of rows, cols, polarcase and polartype from given config file

    Parameters
    ----------
    config_file: str
        Path to config-file
    """
    with open(config_file, 'r') as f:
        lines = f.readlines()
    nrow = int(lines[1])
    ncol = int(lines[4])
    polarcase = lines[7]
    polartype = lines[-1]
    return nrow, ncol, polarcase, polartype


def generate_envi_config(input_path, rows_cols=None, datatype='4'):
    """
    Generates ENVI configuration files for binary data files.

    Args:
        input_path (str): Path to the input directory or file.
        rows_cols (tuple, optional): Tuple containing the number of rows and columns. If not provided, the function reads
                                     the configuration from the 'config.txt' file in the input directory. Default is None.
        datatype (str, optional): The data type of the binary files. Default is '4'.

    Raises:
        IOError: If the input_path does not exist or is invalid.

    Returns:
        None
    """

    if os.path.isfile(input_path):
        filenames = [input_path]
        input_path = os.path.split(input_path)[0]
    elif os.path.isdir(input_path):
        filenames = glob.glob(os.path.join(input_path, '*.bin'))
        filenames.extend(glob.glob(os.path.join(input_path, '*', '*.bin')))
    else:
        raise IOError("Invalid input_path: {}".format(input_path))

    if not rows_cols:
        rows, cols, _, _ = read_config(os.path.join(input_path, 'config.txt'))
    else:
        rows = rows_cols[0]
        cols = rows_cols[1]

    for name in filenames:
        if not os.path.isfile(name + '.hdr'):
            process = subprocess.run(['envi_config_file.exe',
                                      '-bin', name,  # Input directory
                                      '-nam', os.path.basename(name),  # data file name
                                      '-iodf', datatype,  # data type
                                      '-fnr', str(rows),  # final row,
                                      '-fnc', str(cols)],  # final col,
                                     shell=True, capture_output=True,
                                     check=False, cwd=os.path.join(polsarpro, 'tools'))

            print(process.stderr)
            print(process.stdout)


def extract_features_T3(t3_path, output_dir=None, filtersize=(3, 3)):
    """
        Extracts polarimetric features from a coherency matrix stored in t3_path.

        Args:
            t3_path (str): Path to the input directory containing the coherency matrix files.
            output_dir (str, optional): Path to the output directory where the extracted features will be saved.
                                        If not provided, a 'Features' directory will be created inside the t3_path.
            filtersize (tuple, optional): Tuple specifying the filter size as (rows, columns) appllied before
                                          feature extraction. Default is (3, 3).

        Returns:
            str: Path to the output directory containing the extracted features.

    """
    if not output_dir:
        output_dir = os.path.join(t3_path, 'Features')
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    if len(glob.glob(os.path.join(output_dir, '*.bin'))) != 27:

        rows, cols, _, _ = read_config(os.path.join(t3_path, 'config.txt'))
        print('[INFO] H A alpha Decomposition')
        process = subprocess.run(['h_a_alpha_decomposition.exe',  # executable
                                  '-id', t3_path,  # Input directory
                                  '-od', output_dir,  # Output directory
                                  '-iodf', 'T3',  # Inputfile format
                                  '-nwr', str(filtersize[0]),  # filter size row
                                  '-nwc', str(filtersize[1]),  # filter size col
                                  '-ofr', '0',  # Offset row,
                                  '-ofc', '0',  # Offset col,
                                  '-fnr', str(rows),  # final row,
                                  '-fnc', str(cols),  # final col,
                                  '-fl1', '0',
                                  '-fl2', '1',
                                  '-fl3', '1',
                                  '-fl4', '1',
                                  '-fl5', '1',
                                  '-fl6', '0',
                                  '-fl7', '0',
                                  '-fl8', '0',
                                  '-fl9', '0'
                                  ],
                                 shell=True, capture_output=True,
                                 check=False, cwd=os.path.join(polsarpro, 'data_process_sngl'))
        print(process.stderr)
        print(process.stdout)

        process = subprocess.run(['file_operand',
                                  '-if', os.path.join(output_dir, 'lambda.bin'),
                                  '-it', 'float',
                                  '-of', os.path.join(output_dir, 'lambda_db.bin'),
                                  '-ot', 'float',
                                  '-op', '10log',
                                  '-ofr', str(0),  # Offset row,
                                  '-ofc', str(0),  # Offset col,
                                  '-fnr', str(rows),  # final row,
                                  '-fnc', str(cols)  # final col
                                  ],
                                 shell=True, capture_output=True,
                                 check=False, cwd=os.path.join(polsarpro, 'calculator'))

        print('[INFO] Conformity index')
        process = subprocess.run(['conformity_coeff.exe',  # executable
                                  '-id', t3_path,  # Input directory
                                  '-od', output_dir,  # Output directory
                                  '-iodf', 'T3',  # Inputfile format
                                  '-nwr', str(filtersize[0]),  # filter size row
                                  '-nwc', str(filtersize[1]),  # filter size col
                                  '-ofr', str(0),  # Offset row,
                                  '-ofc', str(0),  # Offset col,
                                  '-fnr', str(rows),  # final row,
                                  '-fnc', str(cols)  # final col,
                                  ],
                                 shell=True, capture_output=True,
                                 check=False, cwd=os.path.join(polsarpro, 'data_process_sngl'))
        print(process.stderr)
        print(process.stdout)

        print('[INFO] Eigenvalue set')
        process = subprocess.run(['h_a_alpha_eigenvalue_set.exe',  # executable
                                  '-id', t3_path,  # Input directory
                                  '-od', output_dir,  # Output directory
                                  '-iodf', 'T3',  # Inputfile format
                                  '-nwr', str(filtersize[0]),  # filter size row
                                  '-nwc', str(filtersize[1]),  # filter size col
                                  '-ofr', str(0),  # Offset row,
                                  '-ofc', str(0),  # Offset col,
                                  '-fnr', str(rows),  # final row,
                                  '-fnc', str(cols),  # final col,
                                  '-fl1', '0',
                                  '-fl2', '1',
                                  '-fl3', '0',
                                  '-fl4', '0',
                                  '-fl5', '0',
                                  '-fl6', '0',
                                  '-fl7', '0',
                                  '-fl8', '0',
                                  '-fl9', '0',
                                  '-fl10', '0',
                                  '-fl11', '0',
                                  '-fl12', '0'
                                  ],
                                 shell=True, capture_output=True,
                                 check=False, cwd=os.path.join(polsarpro, 'data_process_sngl'))
        print(process.stderr)
        print(process.stdout)

        print('[INFO] Praks Colin Features')
        process = subprocess.run(['praks_colin.exe',  # executable
                                  '-id', t3_path,  # Input directory
                                  '-od', output_dir,  # Output directory
                                  '-iodf', 'T3',  # Inputfile format
                                  '-nwr', str(filtersize[0]),  # filter size row
                                  '-nwc', str(filtersize[1]),  # filter size col
                                  '-ofr', str(0),  # Offset row,
                                  '-ofc', str(0),  # Offset col,
                                  '-fnr', str(rows),  # final row,
                                  '-fnc', str(cols),  # final col,
                                  '-fl1', '1',
                                  '-fl2', '1',
                                  '-fl3', '1',
                                  '-fl4', '1',
                                  '-fl5', '0',
                                  '-fl6', '0'
                                  ],
                                 shell=True, capture_output=True,
                                 check=False, cwd=os.path.join(polsarpro, 'data_process_sngl'))
        print(process.stderr)
        print(process.stdout)

        print('[INFO] Matrix elements')
        elements = ['11', '12', '13', '22', '23', '33']
        fmt = 'db'
        for elt in elements:
            process = subprocess.run(['process_elements.exe',  # executable
                                      '-id', t3_path,  # Input directory
                                      '-od', output_dir,  # Output directory
                                      '-iodf', 'T3',  # Inputfile format
                                      '-nwr', str(filtersize[0]),  # filter size row
                                      '-nwc', str(filtersize[1]),  # filter size col
                                      '-ofr', str(0),  # Offset row,
                                      '-ofc', str(0),  # Offset col,
                                      '-fnr', str(rows),  # final row,
                                      '-fnc', str(cols),  # final col,
                                      '-elt', elt,  # element index
                                      '-fmt', fmt  # format
                                      ],
                                     shell=True, capture_output=True,
                                     check=False, cwd=os.path.join(polsarpro, 'data_process_sngl'))
            print(process.stderr)
            print(process.stdout)
            # move T-elements into Feature directory
        for t_name in ['T11_db.bin', 'T12_db.bin', 'T13_db.bin', 'T22_db.bin', 'T23_db.bin', 'T33_db.bin']:
            if not os.path.isfile(os.path.join(output_dir, t_name)):
                os.rename(os.path.join(t3_path, t_name),
                          os.path.join(output_dir, t_name))

        print('[INFO] Matrix elements')
        elements = ['12', '13', '23']
        fmt = 'pha'
        for elt in elements:
            process = subprocess.run(['process_elements.exe',  # executable
                                      '-id', t3_path,  # Input directory
                                      '-od', output_dir,  # Output directory
                                      '-iodf', 'T3',  # Inputfile format
                                      '-nwr', str(filtersize[0]),  # filter size row
                                      '-nwc', str(filtersize[1]),  # filter size col
                                      '-ofr', str(0),  # Offset row,
                                      '-ofc', str(0),  # Offset col,
                                      '-fnr', str(rows),  # final row,
                                      '-fnc', str(cols),  # final col,
                                      '-elt', elt,  # element index
                                      '-fmt', fmt  # format
                                      ],
                                     shell=True, capture_output=True,
                                     check=False, cwd=os.path.join(polsarpro, 'data_process_sngl'))
            print(process.stderr)
            print(process.stdout)
        for t_name in ['T12_pha.bin', 'T13_pha.bin', 'T23_pha.bin']:
            if not os.path.isfile(os.path.join(output_dir, t_name)):
                os.rename(os.path.join(t3_path, t_name),
                          os.path.join(output_dir, t_name))

        print('[INFO] Yamaguchi Decomposition')
        process = subprocess.run(['yamaguchi_4components_decomposition.exe',  # executable
                                  '-id', t3_path,  # Input directory
                                  '-od', output_dir,  # Output directory
                                  '-iodf', 'T3',  # Inputfile format
                                  '-mod', 'Y4O',  # decomposition mode
                                  '-nwr', str(filtersize[0]),  # filter size row
                                  '-nwc', str(filtersize[1]),  # filter size col
                                  '-ofr', str(0),  # Offset row,
                                  '-ofc', str(0),  # Offset col,
                                  '-fnr', str(rows),  # final row,
                                  '-fnc', str(cols)  # final col
                                  ],
                                 shell=True, capture_output=True,
                                 check=False, cwd=os.path.join(polsarpro, 'data_process_sngl'))
        print(process.stderr)
        print(process.stdout)
        write_config(output_dir, rows, cols)
        generate_envi_config(output_dir)
    return output_dir


def extract_features_T6(t6_path, output_dir=None, filtersize=(3, 3)):
    """
       Extract polarimetric features from polarimetric interferometric coherency matrix T6.

       Args:
           t6_path (str): Path to the T6 data directory.
           output_dir (str, optional): Output directory to save the extracted features. If not provided,
               a 'Features' directory will be created inside the T6 data directory. Defaults to None.
           filtersize (tuple, optional): Filter size applied before feature extraction. Defaults to (3, 3).

       Returns:
           str: Path to the output directory containing the extracted features.
       """
    if not output_dir:
        output_dir = os.path.join(t6_path, 'Features')
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    # extract coherence sets
    rows, cols, _, _ = read_config(os.path.join(t6_path, 'config.txt'))
    print('[INFO] Coherence set')
    for coh_type in ['HH', 'HV', 'VV', 'HHpVV', 'HHmVV', 'HVpVH', 'LL', 'LR', 'RR', 'HHVV']:
        if not os.path.isfile(os.path.join(output_dir, 'cmplx_coh_' + coh_type + '.bin')):
            process = subprocess.run(['complex_coherence_estimation.exe',  # executable
                                      '-iodf', 'T6',  # Inputfile format
                                      '-id', t6_path,  # Input directory
                                      '-od', output_dir,  # Output directory
                                      '-type', coh_type,
                                      '-nwr', str(filtersize[0]),  # filter size row
                                      '-nwc', str(filtersize[1]),  # filter size col
                                      '-ofr', '0',  # Offset row,
                                      '-ofc', '0',  # Offset col,
                                      '-fnr', str(rows),  # final row,
                                      '-fnc', str(cols),  # final col,
                                      ],
                                     shell=True, capture_output=True,
                                     check=False, cwd=os.path.join(polsarpro, 'data_process_dual'))
            print(process.stderr)
            print(process.stdout)
    if not os.path.isfile(os.path.join(output_dir, 'cmplx_coh_Opt1.bin')):
        process = subprocess.run(['complex_coherence_opt_estimation.exe',  # executable
                                  '-iodf', 'T6',  # Inputfile format
                                  '-id', t6_path,  # Input directory
                                  '-od', output_dir,  # Output directory
                                  '-nwr', str(filtersize[0]),  # filter size row
                                  '-nwc', str(filtersize[1]),  # filter size col
                                  '-ofr', '0',  # Offset row,
                                  '-ofc', '0',  # Offset col,
                                  '-fnr', str(rows),  # final row,
                                  '-fnc', str(cols),  # final col,
                                  ],
                                 shell=True, capture_output=True,
                                 check=False, cwd=os.path.join(polsarpro, 'data_process_dual'))
        print(process.stderr)
        print(process.stdout)
    write_config(output_dir, rows, cols)
    generate_envi_config(output_dir, datatype='8')
    return output_dir


def lee_filter_T3(t3_path, output_dir=None, filtersize=9, looks=3):
    """
    Apply Refined Lee Filtering to the coherency matrix T3.

    Args:
        t3_path (str): Path to the T3 data directory.
        output_dir (str, optional): Output directory to save the filtered T3 matrix.
            If not provided, a 'T3_lee' directory will be created in the parent directory of t3_path.
            Defaults to None.
        filtersize (int, optional): Size of the Lee filter window. Defaults to 9.
        looks (int, optional): Number of looks for filtering. Defaults to 3.

    Returns:
        str: Path to the output directory containing the filtered T3 matrix.
    """
    if not output_dir:
        output_dir = os.path.join(os.path.split(t3_path)[0], 'T3_lee')
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    if not os.path.isfile(os.path.join(output_dir, 'T11.bin')):
        # get rows and cols from confif-file
        rows, cols, _, _ = read_config(os.path.join(t3_path, 'config.txt'))

        print('[INFO] Filtering Covariance matrix (Refined-Lee-Filter)')
        process = subprocess.run(['lee_refined_filter.exe',  # executable
                        '-id', t3_path,  # Input directory
                        '-od', output_dir,  # Output directory
                        '-iodf', 'T3',  # Inputfile format
                        '-nw', str(filtersize),     # filter size
                        '-nlk', str(looks),         # Number of looks
                        '-ofr', '0',  # Offset row,
                        '-ofc', '0',  # Offset col,
                        '-fnr', str(rows),  # final row,
                        '-fnc', str(cols)  # final col,
                        ],
                       shell=True, capture_output=True,
                       check=False, cwd=os.path.join(polsarpro, 'speckle_filter'))
        # Print process output
        print(process.stderr)
        print(process.stdout)
        write_config(output_dir, rows, cols)
        generate_envi_config(output_dir)
    return output_dir


def lee_filter_T6(t6_path, output_dir=None, filtersize=9, looks=3):
    """
    Apply Refined Lee Filtering to the coherency matrix T6.

    Args:
        t6_path (str): Path to the T6 data directory.
        output_dir (str, optional): Output directory to save the filtered T6 matrix.
            If not provided, a 'T6_lee' directory will be created in the parent directory of t3_path.
            Defaults to None.
        filtersize (int, optional): Size of the Lee filter window. Defaults to 9.
        looks (int, optional): Number of looks for filtering. Defaults to 3.

    Returns:
        str: Path to the output directory containing the filtered T6 matrix.
    """
    if not output_dir:
        output_dir = os.path.join(os.path.split(t6_path)[0], 'T6_lee')
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    if not os.path.isfile(os.path.join(output_dir, 'T11.bin')):
        # get rows and cols from confif-file
        rows, cols, _, _ = read_config(os.path.join(t6_path, 'config.txt'))

        print('[INFO] Filtering Covariance matrix (Refined-Lee-Filter)')
        process = subprocess.run(['lee_refined_filter_dual.exe',  # executable
                        '-id', t6_path,  # Input directory
                        '-od', output_dir,  # Output directory
                        '-iodf', 'T6',  # Inputfile format
                        '-nw', str(filtersize),     # filter size
                        '-nlk', str(looks),         # Number of looks
                        '-ofr', '0',  # Offset row,
                        '-ofc', '0',  # Offset col,
                        '-fnr', str(rows),  # final row,
                        '-fnc', str(cols)  # final col,
                        ],
                       shell=True, capture_output=True,
                       check=False, cwd=os.path.join(polsarpro, 'speckle_filter'))
        # Print process output
        print(process.stderr)
        print(process.stdout)
        write_config(output_dir, rows, cols)
        generate_envi_config(output_dir)
    return output_dir


def feature_extraction_pipeline(data_dir, label_dir):
    """
       Extract polarimetric features and interferometric coherences for two flight paths (FP1, FP2) using PolSARPro,
       store the features as flattend lists in a pandas dataframe and save it

       Args:
           data_dir (str): Path to the Pol-InSAR-Island data directory.
           label_dir (str): Path to the Pol-InSAR-Island label directory.

       Returns:
           df (pd.DataFrame): Dataframe containing  flattened  feature images.
       """
    label_train_01 = os.path.join(label_dir, 'FP1', 'label_train.bin')
    label_test_01 = os.path.join(label_dir, 'FP1', 'label_test.bin')
    label_train_02 = os.path.join(label_dir, 'FP2', 'label_train.bin')
    label_test_02 = os.path.join(label_dir, 'FP2', 'label_test.bin')
    t6_01_S = os.path.join(data_dir, 'FP1', 'S', 'T6')
    t6_01_L = os.path.join(data_dir, 'FP1', 'L', 'T6')
    t6_02_S = os.path.join(data_dir, 'FP2', 'S', 'T6')
    t6_02_L = os.path.join(data_dir, 'FP2', 'L', 'T6')

    t6_feature_paths = []
    for t6 in [t6_01_L, t6_01_S, t6_02_L, t6_02_S]:
        t6_feature_paths.append(extract_features_T6(t6))
    # extract T3-Master from T6
    t3_01_S = os.path.join(data_dir, 'FP1', 'S', 'T3')
    t3_01_L = os.path.join(data_dir, 'FP1', 'L', 'T3')
    t3_02_S = os.path.join(data_dir, 'FP2', 'S', 'T3')
    t3_02_L = os.path.join(data_dir, 'FP2', 'L', 'T3')

    t3_feature_paths = []
    for t3 in [t3_01_L, t3_01_S, t3_02_L, t3_02_S]:
        if not os.path.isdir(t3):
            os.makedirs(t3)
            for t_element in ['T11', 'T12_real', 'T12_imag', 'T13_real', 'T13_imag',
                              'T12_imag', 'T22', 'T23_real', 'T23_imag', 'T33']:
                shutil.copyfile(os.path.join(os.path.split(t3)[0], 'T6', t_element + '.bin'),
                                os.path.join(t3, t_element + '.bin'))
                shutil.copyfile(os.path.join(os.path.split(t3)[0], 'T6', t_element + '.bin.hdr'),
                                os.path.join(t3, t_element + '.bin.hdr'))
                rows, cols, _, _ = read_config(os.path.join(os.path.split(t3)[0], 'T6', 'config.txt'))
                write_config(t3, rows, cols)

        # Process polarimetric data
        t3_lee = lee_filter_T3(t3)
        t3_feature_paths.append(extract_features_T3(t3_lee))

    # Create data cube as pandas data frame
    feature_paths_01_L = glob.glob(os.path.join(t3_feature_paths[0], '*.bin'))
    feature_names = [os.path.splitext(os.path.basename(path))[0] + '_L' for path in feature_paths_01_L]
    df_01_L = pd.DataFrame(columns=feature_names,
                           data=np.stack([read_bin(path).flatten() for path in feature_paths_01_L], axis=1))
    df_01_L['coh_hh_L'] = np.abs(read_bin(os.path.join(t6_feature_paths[0], 'cmplx_coh_HH.bin')).flatten())
    df_01_L['coh_hv_L'] = np.abs(read_bin(os.path.join(t6_feature_paths[0], 'cmplx_coh_HV.bin')).flatten())
    df_01_L['coh_vv_L'] = np.abs(read_bin(os.path.join(t6_feature_paths[0], 'cmplx_coh_VV.bin')).flatten())

    feature_paths_01_S = glob.glob(os.path.join(t3_feature_paths[1], '*.bin'))
    feature_names = [os.path.splitext(os.path.basename(path))[0] + '_S' for path in feature_paths_01_S]
    df_01_S = pd.DataFrame(columns=feature_names,
                           data=np.stack([read_bin(path).flatten() for path in feature_paths_01_S], axis=1))
    df_01_S['coh_hh_S'] = np.abs(read_bin(os.path.join(t6_feature_paths[1], 'cmplx_coh_HH.bin')).flatten())
    df_01_S['coh_hv_S'] = np.abs(read_bin(os.path.join(t6_feature_paths[1], 'cmplx_coh_HV.bin')).flatten())
    df_01_S['coh_vv_S'] = np.abs(read_bin(os.path.join(t6_feature_paths[1], 'cmplx_coh_VV.bin')).flatten())

    df_01 = pd.concat([df_01_L, df_01_S], axis=1)
    df_01['label_train'] = read_bin(label_train_01).flatten()
    df_01['label_test'] = read_bin(label_test_01).flatten()

    feature_paths_02_L = glob.glob(os.path.join(t3_feature_paths[2], '*.bin'))
    feature_names = [os.path.splitext(os.path.basename(path))[0] + '_L' for path in feature_paths_02_L]
    df_02_L = pd.DataFrame(columns=feature_names,
                           data=np.stack([read_bin(path).flatten() for path in feature_paths_02_L], axis=1))
    df_02_L['coh_hh_L'] = np.abs(read_bin(os.path.join(t6_feature_paths[2], 'cmplx_coh_HH.bin')).flatten())
    df_02_L['coh_hv_L'] = np.abs(read_bin(os.path.join(t6_feature_paths[2], 'cmplx_coh_HV.bin')).flatten())
    df_02_L['coh_vv_L'] = np.abs(read_bin(os.path.join(t6_feature_paths[2], 'cmplx_coh_VV.bin')).flatten())

    feature_paths_02_S = glob.glob(os.path.join(t3_feature_paths[3], '*.bin'))
    feature_names = [os.path.splitext(os.path.basename(path))[0] + '_S' for path in feature_paths_02_S]
    df_02_S = pd.DataFrame(columns=feature_names,
                           data=np.stack([read_bin(path).flatten() for path in feature_paths_02_S], axis=1))
    df_02_S['coh_hh_S'] = np.abs(read_bin(os.path.join(t6_feature_paths[3], 'cmplx_coh_HH.bin')).flatten())
    df_02_S['coh_hv_S'] = np.abs(read_bin(os.path.join(t6_feature_paths[3], 'cmplx_coh_HV.bin')).flatten())
    df_02_S['coh_vv_S'] = np.abs(read_bin(os.path.join(t6_feature_paths[3], 'cmplx_coh_VV.bin')).flatten())

    df_02 = pd.concat([df_02_L, df_02_S], axis=1)
    df_02['label_train'] = read_bin(label_train_02).flatten()
    df_02['label_test'] = read_bin(label_test_02).flatten()

    df = pd.concat([df_01, df_02], axis=0)
    for band in ['S', 'L']:
        yama_span = df['Yamaguchi4_Y4O_Vol_' + band] + df['Yamaguchi4_Y4O_Dbl_' + band] + \
                    df['Yamaguchi4_Y4O_Odd_' + band] + df['Yamaguchi4_Y4O_Hlx_' + band]
        df['Yamaguchi4_Vol_rel_' + band] = df['Yamaguchi4_Y4O_Vol_' + band] / yama_span
        df['Yamaguchi4_Dbl_rel_' + band] = df['Yamaguchi4_Y4O_Dbl_' + band] / yama_span
        df['Yamaguchi4_Odd_rel_' + band] = df['Yamaguchi4_Y4O_Odd_' + band] / yama_span
        df['Yamaguchi4_Hlx_rel_' + band] = df['Yamaguchi4_Y4O_Hlx_' + band] / yama_span
        df[['Yamaguchi4_Odd_rel_' + band, 'Yamaguchi4_Dbl_rel_' + band, 'Yamaguchi4_Vol_rel_' + band]] = \
            df[['Yamaguchi4_Odd_rel_' + band, 'Yamaguchi4_Dbl_rel_' + band, 'Yamaguchi4_Vol_rel_' + band]].fillna(0)
        del df['lambda_' + band], df['l1_' + band]
    df.reset_index().to_feather(os.path.join(data_dir, 'df.ftr'))
    return df


if __name__ == '__main__':

    data_dir = sys.argv[2]
    label_dir = sys.argv[3]
    feature_extraction_pipeline(data_dir, label_dir)

import numpy as np
import os
import glob
import sys
import pickle as pkl
from feature_extraction import read_bin, read_config, lee_filter_T6
from sklearn.metrics import confusion_matrix
import random
import argparse

np.random.seed(42)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-b", "--balanced_samples", help="Choose if the classifier should be trained on a class"
                                                         "balanced subset of samples (True) or on all training samples"
                                                         "(False, default)", required=False, default=False)
    return parser.parse_args()


def supervised_wishart(t6_matrix, label, max_iteration=1):
    """Perform supervised Wishart classification"""
    # begin iterative process
    for iter in range(max_iteration):
        # calculate center means
        nclass = label.max()
        cc = []
        nn = []
        for c in range(1, nclass + 1):
            cov_c = t6_matrix[label == c, ...]
            nn.append(cov_c.shape[0]) # number of members
            if cov_c.shape[0] > 0:
                cc.append(np.mean(cov_c, axis=0))
            else:
                cc.append(np.zeros((t6_matrix.shape[1], t6_matrix.shape[2]), dtype=cov_c.dtype))

        # remove empty classes
        cc = [cc[i] for i, n in enumerate(nn) if n != 0]

        # Assign classes
        dist = np.empty((t6_matrix.shape[0], len(cc)))
        for i, c in enumerate(cc):
            d = np.einsum('ij,...jk->...ik', np.linalg.inv(c), t6_matrix)
            dist[..., i] = np.abs(np.trace(d, axis1=1, axis2=2)) \
                       + np.log(np.abs(np.linalg.det(c)))
        label = np.ndarray.astype(np.argmin(dist, axis=1), dtype=np.ubyte)
    return label


def supervised_wishart_balanced(t6_matrix, label, max_iteration=1):
    """Perform supervised Wishart classification"""
    # begin iterative process
    for iter in range(max_iteration):
        # calculate center means on balanced data subset
        nclass = label.max()
        unique, counts = np.unique(label, return_counts=True)
        min_label = counts.min()
        cc = []
        nn = []
        for c in range(1, nclass + 1):
            cov_c = t6_matrix[label == c, ...]
            if cov_c.shape[0] > min_label:
                cov_c = cov_c[list(np.random.randint(cov_c.shape[0], size=min_label)), ...]
            nn.append(cov_c.shape[0]) # number of members
            if cov_c.shape[0] > 0:
                cc.append(np.mean(cov_c, axis=0))
            else:
                cc.append(np.zeros((t6_matrix.shape[1], t6_matrix.shape[2]), dtype=cov_c.dtype))

        # remove empty classes
        cc = [cc[i] for i, n in enumerate(nn) if n != 0]

        # Assign classes
        dist = np.empty((t6_matrix.shape[0], len(cc)))
        for i, c in enumerate(cc):
            d = np.einsum('ij,...jk->...ik', np.linalg.inv(c), t6_matrix)
            dist[..., i] = np.abs(np.trace(d, axis1=1, axis2=2)) \
                       + np.log(np.abs(np.linalg.det(c)))
        label = np.ndarray.astype(np.argmin(dist, axis=1), dtype=np.ubyte)
    return label


if __name__ == '__main__':
    data_dir = os.path.join(sys.argv[2], 'data')
    label_dir = os.path.join(sys.argv[2], 'label')
    output_dir = sys.argv[3]
    balanced_samples = parse_args().balanced_samples

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
        # extract coherence sets
    label_train_01 = os.path.join(label_dir, 'FP1', 'label_train.bin')
    label_test_01 = os.path.join(label_dir, 'FP1', 'label_test.bin')
    label_train_02 = os.path.join(label_dir, 'FP2', 'label_train.bin')
    label_test_02 = os.path.join(label_dir, 'FP2', 'label_test.bin')
    t6_01_S = os.path.join(data_dir, 'FP1', 'S', 'T6')
    t6_01_L = os.path.join(data_dir, 'FP1', 'L', 'T6')
    t6_02_S = os.path.join(data_dir, 'FP2', 'S', 'T6')
    t6_02_L = os.path.join(data_dir, 'FP2', 'L', 'T6')

    # Lee-filter
    for t6_path in [t6_02_S, t6_02_L, t6_01_S, t6_01_L]:
        if not os.path.isdir(t6_path + '_lee'):
            lee_filter_T6(t6_path)

    t_names = ['T11', 'T12_real', 'T12_imag', 'T13_real', 'T13_imag', 'T14_real', 'T14_imag',
                  'T15_real', 'T15_imag', 'T16_real', 'T16_imag', 'T22', 'T23_real', 'T23_imag',
                  'T24_real', 'T24_imag', 'T25_real', 'T25_imag', 'T26_real', 'T26_imag',
                  'T33', 'T34_real', 'T34_imag', 'T35_real', 'T35_imag', 'T36_real', 'T36_imag',
                  'T44', 'T45_real', 'T45_imag', 'T46_real', 'T46_imag',
                  'T55', 'T56_real', 'T56_imag', 'T66']

    # Wishart classifier for S and L band data
    results = []
    for band in ['S', 'L']:
        # generate concatenated T6 matrix for S- and L-band
        rows_01, cols_01, _, _ = read_config(os.path.join(data_dir, 'FP1', band, 'T6_lee', 'config.txt'))
        rows_02, cols_02, _, _ = read_config(os.path.join(data_dir, 'FP2', band, 'T6_lee', 'config.txt'))

        # generate complex T6 matrix from files
        t_elements_01 = [read_bin(os.path.join(os.path.join(data_dir, 'FP1', band, 'T6_lee'), t + '.bin')).flatten()
                         for t in t_names]
        t_elements_02 = [read_bin(os.path.join(os.path.join(data_dir, 'FP2', band, 'T6_lee'), t + '.bin')).flatten()
                         for t in t_names]
        labels = np.concatenate([read_bin(label_train_01).flatten(), read_bin(label_train_02).flatten()])
        t_elements = [np.concatenate([t_01, t_02]) for t_01, t_02 in zip(t_elements_01, t_elements_02)]

        t6_matrix = np.empty((t_elements[0].shape[0], 6, 6), dtype=np.complex64)
        t6_matrix[:, 0, 0] = t_elements[0]
        t6_matrix[:, 0, 1] = t_elements[1] + 1j * t_elements[2]
        t6_matrix[:, 0, 2] = t_elements[3] + 1j * t_elements[4]
        t6_matrix[:, 0, 3] = t_elements[5] + 1j * t_elements[6]
        t6_matrix[:, 0, 4] = t_elements[7] + 1j * t_elements[8]
        t6_matrix[:, 0, 5] = t_elements[9] + 1j * t_elements[10]
        t6_matrix[:, 1, 0] = np.conj(t6_matrix[:, 0, 1])
        t6_matrix[:, 1, 1] = t_elements[11]
        t6_matrix[:, 1, 2] = t_elements[12] + 1j * t_elements[13]
        t6_matrix[:, 1, 3] = t_elements[14] + 1j * t_elements[15]
        t6_matrix[:, 1, 4] = t_elements[16] + 1j * t_elements[17]
        t6_matrix[:, 1, 5] = t_elements[18] + 1j * t_elements[19]
        t6_matrix[:, 2, 0] = np.conj(t6_matrix[:, 0, 2])
        t6_matrix[:, 2, 1] = np.conj(t6_matrix[:, 1, 2])
        t6_matrix[:, 2, 2] = t_elements[20]
        t6_matrix[:, 2, 3] = t_elements[21] + 1j * t_elements[22]
        t6_matrix[:, 2, 4] = t_elements[23] + 1j * t_elements[24]
        t6_matrix[:, 2, 5] = t_elements[25] + 1j * t_elements[26]
        t6_matrix[:, 3, 0] = np.conj(t6_matrix[:, 0, 3])
        t6_matrix[:, 3, 1] = np.conj(t6_matrix[:, 1, 3])
        t6_matrix[:, 3, 2] = np.conj(t6_matrix[:, 2, 3])
        t6_matrix[:, 3, 3] = t_elements[27]
        t6_matrix[:, 3, 4] = t_elements[28] + 1j * t_elements[29]
        t6_matrix[:, 3, 5] = t_elements[30] + 1j * t_elements[31]
        t6_matrix[:, 4, 0] = np.conj(t6_matrix[:, 0, 4])
        t6_matrix[:, 4, 1] = np.conj(t6_matrix[:, 1, 4])
        t6_matrix[:, 4, 2] = np.conj(t6_matrix[:, 2, 4])
        t6_matrix[:, 4, 3] = np.conj(t6_matrix[:, 3, 4])
        t6_matrix[:, 4, 4] = t_elements[32]
        t6_matrix[:, 4, 5] = t_elements[33] + 1j * t_elements[34]
        t6_matrix[:, 5, 0] = np.conj(t6_matrix[:, 0, 5])
        t6_matrix[:, 5, 1] = np.conj(t6_matrix[:, 1, 5])
        t6_matrix[:, 5, 2] = np.conj(t6_matrix[:, 2, 5])
        t6_matrix[:, 5, 3] = np.conj(t6_matrix[:, 3, 5])
        t6_matrix[:, 5, 4] = np.conj(t6_matrix[:, 4, 5])
        t6_matrix[:, 5, 5] = t_elements[35]

        # Wishart classification
        if balanced_samples:
            wishart_map = supervised_wishart_balanced(t6_matrix, labels)
        else:
            wishart_map = supervised_wishart(t6_matrix, labels)
        # Save results
        results.append(wishart_map)
        np.save(os.path.join(output_dir, 'wishart_01_' + band + '.npy'),
                np.reshape(wishart_map[:rows_01 * cols_01], (rows_01, cols_01)))
        np.save(os.path.join(output_dir, 'wishart_02_' + band + '.npy'),
                np.reshape(wishart_map[rows_01 * cols_01:], (rows_02, cols_02)))

    # evaluate results
    test_label = np.concatenate([read_bin(label_test_01).flatten(), read_bin(label_test_02).flatten()])
    y_true = test_label[test_label != 0]
    for map, band in zip(results, ['S', 'L']):
        y_pred = map[test_label != 0]
        cm = confusion_matrix(y_true, y_pred)
        with open(os.path.join(output_dir, 'wishart_' + band + '.pkl'), 'wb') as f:
            pkl.dump(cm, f)
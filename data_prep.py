import os
import numpy as np
import re
import io


rx_dict = {
    'ncol': re.compile(r'samples += (?P<ncol>\d+)'),
    'nrow': re.compile(r'lines += (?P<nrow>\d+)'),
    'ndim': re.compile(r'bands += (?P<ndim>\d)'),
    'datatype': re.compile(r'data type += (?P<datatype>\d+)')
}

datatype_dict = {
    1: np.dtype('uint8'),
    2: np.int,
    3: np.long,
    4: np.float32,
    5: np.float64,
    6: np.complex,
    8: np.complex64
}


def _parse_line(line):
    """
    Do a regex search against all defined regexes and
    return the key and match result of the first matching regex

    """

    for key, rx in rx_dict.items():
        match = rx.search(line)
        if match:
            return key, match
    # if there are no matches
    return None, None


def parse_hdr_header(filename):
    """ Parsing image dimension and datatype from hdr-file

    Parameters
    ----------
    filename: str
        Path to hdr-File

    Returns
    -------
    ncol: int
        number of cols
    nrow: int
        number of rows
    ndim: int
        number of channels
    datatype: np.dtype
        datatype
    """
    with io.open(filename, 'rt', newline='\n') as hdr:
        lines = hdr.readlines()
        for line in lines:
            key, match = _parse_line(line)
            if key == 'ncol':
                ncol = int((match.group('ncol')))
            if key == 'nrow':
                nrow = int(match.group('nrow'))
            if key =='datatype':
                datatype = datatype_dict[int(match.group('datatype'))]
            if key == 'ndim':
                ndim = int(match.group('ndim'))
    return ncol, nrow, ndim, datatype


def read_bin(input_file):
    """ Read binary files (.bin)

    Parameters
    ----------
    input_file: str
        Path to .bin-file

    Return
    ------
    arr: np.array
        numpy array with dimension and type specified in corresponding hdr-file.
    """

    if os.path.splitext(input_file)[-1] != '.bin':
        raise FileNotFoundError
    hdr_file = input_file + '.hdr'
    ncol, nrow, ndim, datatype = parse_hdr_header(hdr_file)
    with open(input_file, 'rb') as f:
        b = f.read()
    return np.frombuffer(b, dtype=datatype).reshape((nrow, ncol))


def flat_earth_removal(input_dir_t6, path_fe, output_dir_t6):
    """ Remove flat earth component from T6 matrix
    Parameters
    ----------
    input_dir_t6: str
        Path to T6 directory
    path_fe:
        Path to flat earth phase file
    output_dir_t6:
        Path to output directory for corrected T6 files
    """
    # Load data and create ouput directory
    fe = read_bin(path_fe)
    if not os.path.isdir(output_dir_t6):
        os.makedirs(output_dir_t6)

    for t_ij in ['T14', 'T15', 'T16', 'T24', 'T25', 'T26', 'T34', 'T35', 'T36']:
        tij_real = read_bin(os.path.join(input_dir_t6, t_ij + '_real.bin'))
        tij_imag = read_bin(os.path.join(input_dir_t6, t_ij + '_imag.bin'))

        tij_rho = np.sqrt(tij_real**2 + tij_imag**2)
        tij_phi = np.arctan2(tij_imag, tij_real) - fe
        tij_real_fer = tij_rho * np.cos(tij_phi)
        tij_imag_fer = tij_rho * np.sin(tij_phi)
        with open(os.path.join(output_dir_t6, t_ij + '_real.bin'), 'wb') \
                as bin_file:
            tij_real_fer.astype('float32').tofile(bin_file)
        with open(os.path.join(output_dir_t6, t_ij + '_imag.bin'), 'wb') \
                as bin_file:
            tij_imag_fer.astype('float32').tofile(bin_file)




